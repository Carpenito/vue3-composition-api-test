# vue-composition-api-example

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

## App Online


[https://vue3-composition-api-test.vercel.app/](https://vue3-composition-api-test.vercel.app/)


